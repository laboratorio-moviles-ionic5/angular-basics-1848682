import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  title = 'angular-basics-1848692';
  alumnosDummy: string[] = ['Beatriz', 'Barbara', 'Brandon'];
  
  onAddAlumno(name: string){
    this.alumnosDummy.push(name);
  }
}
