import { Component} from '@angular/core';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { AlumnoService } from './alumno.service';
import { Subscription } from 'rxjs';

@Component({
    selector: 'app-alumno',

    import { AlumnoService } from './alumno.service';
    styleUrls: ['./alumno.component.css']

})    
    export class AlumnoComponent{
    export class AlumnoComponent implements OnInit, OnDestroy{

        private alumnosSubs: Subscription;
    
        activo: boolean = true;
        consultando: boolean = true;
        alumnos: string[];

        constructor(service: AlumnoService){
            this.alumnos = service.alumnosDummy;
            constructor(private service: AlumnoService){
                //this.alumnos = service.alumnosDummy;
            }
        
            ngOnInit(){
                this.alumnos = this.service.alumnos;
                //this.alumnosSubs = this.service.alumnosChanged.suscibe(alumnos => {
                this.service.fetchAlumnos();    
                this.alumnos = alumnos;
                this.consultando = false;
                    });
                }

                ngOnDestroy(){
                    this.alumnosSubs.unsubscribe();
                }


                onCLickActivar(){
                    this.activo = ! this.activo;
                }

                onRemoveAlumno(name: string){
                    this.service.removeAlumno(name);
                }
            


        //activo: boolean = true;
        //Input() alumnos: string[] = ['Arturo','Astrid','Adriana'];
        //alumnos: string[];

        //constructor(service: AlumnoService){
          //  this.alumnos = service.alumnosDummy;
        }
        
        





