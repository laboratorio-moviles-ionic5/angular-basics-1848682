import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({providedIn: 'root'})

export class AlumnoService{
    //alumnosDummy: string[] = ['Carlos','Cecilia','Carmen'];
    alumnosChanged = new Subject<string[]>();
    alumnos: string [] = ['Carlos','Cecilia','Carmen'];
    alumnos: string [] = [];

    constructor(private http: HttpClient){}

    fetchAlumnos(){
        this.http.get<any>('https://swapi.dev/api/people/').pipe(map(response =>{
            return response.results.map(obj => obj.name)
            }))
            .subscribe(response => {
                console.warn(response);
                this.alumnos = (rsponse);
                this.alumnosChanged.next(this.alumnos);
                });
    }

    onAddAlumno(name: string){
        this.alumnosDummy.push(name);
        addAlumno(name: string){
            this.alumnos.push(name);
            this.alumnosChanged.next(this.alumnos);
        }

        removeAlumno(name: string){
            this.alumnos = this.alumnos.filter(alumno=>{
            return alumno !== name;
            });
        }
    }
}